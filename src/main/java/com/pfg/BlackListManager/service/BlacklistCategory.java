package com.pfg.BlackListManager.service;

import com.pfg.BlackListManager.model.Blacklist;
import com.pfg.BlackListManager.model.BlacklistCount;
import com.pfg.BlackListManager.repository.BlackListCountRepository;
import com.pfg.BlackListManager.repository.BlackListRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class BlacklistCategory {
    private static final Logger log = LoggerFactory.getLogger(BlacklistCategory.class);

    @Autowired
    BlackListRepository blackListRepository;

    @Autowired
    BlackListCountRepository blackListCountRepository;

    @Async
    public CompletableFuture findBlackListedCategory(String fieldName, String fieldValue) {
        if (fieldValue != null && !StringUtils.isEmpty(fieldValue)) {
            List<Blacklist> list = blackListRepository.findByField(fieldName, fieldValue);
            return CompletableFuture.completedFuture(list);
        }
        return null;
    }

    @Async
    public CompletableFuture findBlackListedCategory(String fieldName, Long fieldValue) {
        if (fieldValue != null && !StringUtils.isEmpty(fieldValue)) {
            List<Blacklist> list = blackListRepository.findByField(fieldName, fieldValue);
            return CompletableFuture.completedFuture(list);
        }
        return null;
    }

    @Async
    public void asyncCall(Long maskId, String deviceId, String email, String customerId, long count) throws InterruptedException, java.util.concurrent.ExecutionException {
        if (count > 0) {
            BlacklistCount blacklist = new BlacklistCount();
            int deviceIdSize = 0;
            int customerIdSize = 0;
            int emailIdSize = 0;
            CompletableFuture<List<Blacklist>> listByDeviceId = findBlackListedCategory("deviceId", deviceId);
            if (listByDeviceId != null) {
                deviceIdSize = listByDeviceId.get().size();
                if (deviceIdSize > 0) {
                    Set<Long> deviceIds = listByDeviceId.get().stream().map(Blacklist::getMaskId).collect(Collectors.toSet());
                    HashSet<String> deviceIdReasonSet = new HashSet<>();
                    deviceIds.forEach(em -> {
                        String s = em.toString();
                        deviceIdReasonSet.add(s);
                        blacklist.setReasonDeviceId(deviceIdReasonSet);
                    });
                }
                log.debug("Blacklisted by Device id : " + deviceId + " " + deviceIdSize);
            } else {
                blacklist.setReasonDeviceId(new HashSet<>());
            }
            CompletableFuture<List<Blacklist>> listByCustomerId = findBlackListedCategory("customerId", customerId);
            if (listByCustomerId != null) {
                customerIdSize = listByCustomerId.get().size();
                if (customerIdSize > 0) {
                    Set<Long> customerIds = listByCustomerId.get().stream().map(Blacklist::getMaskId).collect(Collectors.toSet());
                    HashSet<String> customerIdReasonSet = new HashSet<>();
                    customerIds.forEach(customId -> {
                        String s = customId.toString();
                        customerIdReasonSet.add(s);
                        blacklist.setReasonCustomerId(customerIdReasonSet);
                    });
                }
                log.debug("Blacklisted by Customer id : " + customerId + " " + customerIdSize);
            } else {
                blacklist.setReasonCustomerId(new HashSet<>());
            }
            CompletableFuture<List<Blacklist>> listByEmail = findBlackListedCategory("email", email);
            if (listByEmail != null) {
                emailIdSize = listByEmail.get().size();
                if (emailIdSize > 0) {
                    Set<Long> emails = listByEmail.get().stream().map(Blacklist::getMaskId).collect(Collectors.toSet());
                    HashSet<String> emailReasonSet = new HashSet<>();
                    emails.forEach(em -> {
                        String s = em.toString();
                        emailReasonSet.add(s);
                        blacklist.setReasonEmail(emailReasonSet);
                    });
                }
                log.debug("Blacklisted by Email id : " + email + " " + emailIdSize);
            } else {
                blacklist.setReasonEmail(new HashSet<>());
            }


            blacklist.setMaskId(checkForNull(maskId));
            blacklist.setDeviceId(checkForNull(deviceId));
            blacklist.setEmail(checkForNull(email));
            blacklist.setCustomerId(checkForNull(customerId));
            blacklist.setDeviceIdCount(deviceIdSize);
            blacklist.setCustomerIdCount(customerIdSize);
            blacklist.setEmailCount(emailIdSize);
            blacklist.setCreatedDate(new Date());
            blackListCountRepository.save(blacklist);
            // Join all threads so that we can wait until all are done
            //CompletableFuture.allOf(listByDeviceId, listByCustomerId, listByEmail,listByMaskId).join();

        }
    }

    private String checkForNull(String value) {
        return (!StringUtils.isEmpty(value)) ? value : null;
    }

    private Long checkForNull(Long value) {
        return value != null ? value : null;
    }
}
