package com.pfg.BlackListManager.repository;

import java.util.Date;
import java.util.List;

import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.pfg.BlackListManager.model.Blacklist;
import org.springframework.util.StringUtils;

@Repository
public class BlackListRepository   {
	
	@Autowired
    private MongoTemplate mongoTemplate;
	
	public List<Blacklist> findAll() {
		return mongoTemplate.findAll(Blacklist.class);
	}

	public Blacklist save(Blacklist blacklist) {
		blacklist.setCreatedDate(new Date());
		mongoTemplate.save(blacklist);
		return blacklist;
	}
	
	public List<Blacklist> find(Long maskId,String email,String deviceId,String customerId){
		
		Criteria criteria = new Criteria();
		criteria.orOperator(Criteria.where("maskId").is(checkForNull(maskId)),
				Criteria.where("email").is(checkForNull(email)),
				Criteria.where("deviceId").is(checkForNull(deviceId)),
				Criteria.where("customerId").is(checkForNull(customerId)));
        Query query = new Query(criteria);
		return mongoTemplate.find(query, Blacklist.class);
    }
	private String checkForNull(String value) {
		return (!StringUtils.isEmpty(value))?value:null;
	}
	private Long checkForNull(Long value) {
		return value!=null?value:null;
	}
	public long findCount(Long maskId, String email, String deviceId, String customerId){

		Criteria criteria = new Criteria();
		criteria.orOperator(Criteria.where("maskId").is(maskId),
				Criteria.where("email").is(email),
				Criteria.where("deviceId").is(deviceId),
				Criteria.where("customerId").is(customerId));

		Query query = new Query(criteria);
		return mongoTemplate.count(query, Blacklist.class);
	}
	public List<Blacklist> findByField(String fieldName,String fieldValue){

		Criteria criteria = new Criteria();
		if(!StringUtils.isEmpty(fieldValue)) {
			criteria.orOperator(
					Criteria.where(fieldName).is(fieldValue));
		}
		Query query = new Query(criteria);
		return mongoTemplate.find(query, Blacklist.class);
	}
	public List<Blacklist> findByField(String fieldName,Long fieldValue){

		Criteria criteria = new Criteria();
		if(!StringUtils.isEmpty(fieldValue)) {
			criteria.orOperator(
					Criteria.where(fieldName).is(fieldValue));
		}
		Query query = new Query(criteria);
		return mongoTemplate.find(query, Blacklist.class);
	}

	public DeleteResult  delete(Long maskId,String email,String deviceId,String customerId){

		Criteria criteria = new Criteria();
		criteria.orOperator(Criteria.where("maskId").is(checkForNull(maskId)),
				Criteria.where("email").is(checkForNull(email)),
				Criteria.where("deviceId").is(checkForNull(deviceId)),
				Criteria.where("customerId").is(checkForNull(customerId)));
		Query query = new Query(criteria);
		DeleteResult result = mongoTemplate.remove(query, Blacklist.class);
		if (result.wasAcknowledged()) {
			return DeleteResult.acknowledged(result.getDeletedCount());
		} else {
			return DeleteResult.unacknowledged();
		}
		//return mongoTemplate.remove(query, Blacklist.class);
	}

	public DeleteResult  delete(Long maskId){

		Criteria criteria = new Criteria();
		criteria.orOperator(Criteria.where("maskId").is(checkForNull(maskId)));
		Query query = new Query(criteria);
		DeleteResult result = mongoTemplate.remove(query, Blacklist.class);
		if (result.wasAcknowledged()) {
			return DeleteResult.acknowledged(result.getDeletedCount());
		} else {
			return DeleteResult.unacknowledged();
		}
		//return mongoTemplate.remove(query, Blacklist.class);
	}
}
