package com.pfg.BlackListManager.repository;

import com.pfg.BlackListManager.model.Blacklist;
import com.pfg.BlackListManager.model.BlacklistCount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

@Repository
public class BlackListCountRepository {
	
	@Autowired
    private MongoTemplate mongoTemplate;
	
	public List<BlacklistCount> findAll() {
		return mongoTemplate.findAll(BlacklistCount.class);
	}

	public BlacklistCount save(BlacklistCount blacklistCount) {
		mongoTemplate.save(blacklistCount);return blacklistCount;
	}



	public List<BlacklistCount> find(Long maskId) {

		Criteria criteria = new Criteria();
		criteria.orOperator(Criteria.where("maskId").is(maskId));
		Query query = new Query(criteria);
		return mongoTemplate.find(query, BlacklistCount.class);
	}
	public List<BlacklistCount> findByField(Long maskId){

		Criteria criteria = new Criteria();
		criteria.orOperator(Criteria.where("reasonDeviceId").is(checkForNull(maskId.toString())),
				Criteria.where("reasonCustomerId").is(checkForNull(maskId.toString())),
				Criteria.where("reasonEmail").is(checkForNull(maskId.toString())));
		Query query = new Query(criteria);
		return mongoTemplate.find(query, BlacklistCount.class);
	}
	private String checkForNull(String value) {
		return value!=null?value:null;
	}
}
