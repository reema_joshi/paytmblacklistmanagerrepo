package com.pfg.BlackListManager.controller;

import java.util.List;

import com.mongodb.client.result.DeleteResult;
import com.pfg.BlackListManager.model.BlacklistCount;
import com.pfg.BlackListManager.repository.BlackListCountRepository;
import com.pfg.BlackListManager.response.Response;
import com.pfg.BlackListManager.service.BlacklistCategory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pfg.BlackListManager.model.Blacklist;
import com.pfg.BlackListManager.repository.BlackListRepository;

@RestController
@RequestMapping("/api")
public class BlackListController {

    private static final Logger log = LoggerFactory.getLogger(BlackListController.class);

    @Autowired
    BlackListRepository blackListRepository;

    @Autowired
    BlackListCountRepository blackListCountRepository;


    @Autowired
    BlacklistCategory blacklistCategory;

    @PostMapping("/blacklist")
    public ResponseEntity<Blacklist> createBlacklist(@RequestBody Blacklist blacklist) {
        log.debug("Processing create blacklist request");
        try {
            Blacklist _blacklist = blackListRepository.save(blacklist);
            log.debug("Blacklist entry successfully saved : " + _blacklist.getId());

            return new ResponseEntity<>(_blacklist, HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("Blacklist entry failed to saved : ", e);

            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }

    }

    @PostMapping("/blacklists")
    public ResponseEntity<Response> createBlacklists(@RequestBody List<Blacklist> blacklists) {

        log.debug("Processing create blacklist request");
        ResponseEntity<Response> response = null;
        try {
            blacklists.forEach(
                    blacklist -> {
                        blackListRepository.save(blacklist);
                        log.debug("Blacklist entry successfully saved : " + blacklist.getId());
                    });
            response = ResponseEntity.accepted().body(prepareSuccessResponse(0));

        } catch (Exception e) {
            log.error("Blacklist entry failed to saved : ", e);
            response = ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(prepareFailureResponse(e));
        }
        return response;
    }

    @GetMapping("/blacklist")
    public ResponseEntity<Response> getBlacklist(@RequestParam(required = false) Long maskId,
                                                 @RequestParam(required = false) String deviceId,
                                                 @RequestParam(required = false) String email,
                                                 @RequestParam(required = false) String customerId) {

        Boolean blocked = Boolean.FALSE;
        log.debug("Processing get blacklist request");

        ResponseEntity<Response> response = null;

        try {
            //List<Blacklist> _blacklist = blackListRepository.find(maskId, email, deviceId, customerId);
            long count = blackListRepository.findCount(maskId, email, deviceId, customerId);
            log.debug("Blacklist entry is loaded: " + count);
            blacklistCategory.asyncCall(maskId, deviceId, email, customerId, count);
            response = ResponseEntity.accepted().body(prepareSuccessResponse(count));


        } catch (Exception e) {
            log.error("get blacklist called failed.", e);
            response = ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(prepareFailureResponse(e));

        }

        return response;

    }

    /**
     *
     * @param maskId
     * @param deviceId
     * @param email
     * @param customerId
     * @return
     */
    @GetMapping("/blacklist/delete")
    ResponseEntity<List<BlacklistCount>> deleteBlacklists(@RequestParam(required = false) Long maskId,
                                                          @RequestParam(required = false) String deviceId,
                                                          @RequestParam(required = false) String email,
                                                          @RequestParam(required = false) String customerId) {
        ResponseEntity<BlacklistCount> response = null;
        List<BlacklistCount> reasonBlacklistOfMaskId =null;
        try {
            reasonBlacklistOfMaskId = blackListCountRepository.find(maskId);
            /*DeleteResult delete = blackListRepository.delete(maskId, deviceId, email, customerId);*/
            DeleteResult delete =blackListRepository.delete(maskId);
            long deletedCount = delete.getDeletedCount();
            log.debug("Blacklist entries deleted count: " + deletedCount);
            //response = ResponseEntity.accepted().body(new Response("SUCCESS",deletedCount));
            if(deletedCount>0) {
                return new ResponseEntity<>(reasonBlacklistOfMaskId, HttpStatus.OK);
            }
            else
                return new ResponseEntity<>(reasonBlacklistOfMaskId, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Delete Blacklist api call failed.",e);
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    private Response prepareFailureResponse(Exception e) {
        Response response = new Response();

        response.setStatus("FAIL");
        response.setBlocked(Boolean.FALSE);
        response.setBlacklisted(0);
        response.setMessage(e.getMessage());

        return response;
    }

    private Response prepareSuccessResponse(long blacklistSize) {
        Response response = new Response();

        response.setStatus("SUCCESS");
        response.setBlocked(blacklistSize > 0);
        response.setBlacklisted(blacklistSize);

        return response;
    }
}
