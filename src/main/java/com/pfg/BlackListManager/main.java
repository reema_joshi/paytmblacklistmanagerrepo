package com.pfg.BlackListManager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pfg.BlackListManager.model.BlacklistCount;

public class main {
    public static void main(String[] args) {
        // Creating Object of ObjectMapper define in Jakson Api
        ObjectMapper Obj = new ObjectMapper();
        try {

            // get Oraganisation object as a json string
            BlacklistCount blacklistCount=new BlacklistCount();
           // blacklistCount.setBlacklistReason(new BlacklistReason());
            String jsonStr = Obj.writeValueAsString(blacklistCount);
            System.out.println(jsonStr);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
