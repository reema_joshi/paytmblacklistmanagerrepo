package com.pfg.BlackListManager.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Set;

@Document(collection = "blacklist_count")
public class BlacklistCount {

    @Id
    private String id;

    @Indexed
    private Long maskId;


    @Indexed
    private String deviceId;

    @Indexed
    private String email;

    @Indexed
    private String customerId;

    @Indexed
    private int emailCount;

    @Indexed
    private int deviceIdCount;

    @Indexed
    private int customerIdCount;

    @Indexed
    private Set<String> reasonEmail;

    @Indexed
    private Set<String> reasonDeviceId;

    @Indexed
    private Set<String> reasonCustomerId;

    @Indexed
    private Date createdDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getMaskId() {
        return maskId;
    }

    public void setMaskId(Long maskId) {
        this.maskId = maskId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public int getEmailCount() {
        return emailCount;
    }

    public void setEmailCount(int emailCount) {
        this.emailCount = emailCount;
    }

    public int getDeviceIdCount() {
        return deviceIdCount;
    }

    public void setDeviceIdCount(int deviceIdCount) {
        this.deviceIdCount = deviceIdCount;
    }

    public int getCustomerIdCount() {
        return customerIdCount;
    }

    public void setCustomerIdCount(int customerIdCount) {
        this.customerIdCount = customerIdCount;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Set<String> getReasonEmail() {
        return reasonEmail;
    }

    public void setReasonEmail(Set<String> reasonEmail) {
        this.reasonEmail = reasonEmail;
    }

    public Set<String> getReasonDeviceId() {
        return reasonDeviceId;
    }

    public void setReasonDeviceId(Set<String> reasonDeviceId) {
        this.reasonDeviceId = reasonDeviceId;
    }

    public Set<String> getReasonCustomerId() {
        return reasonCustomerId;
    }

    public void setReasonCustomerId(Set<String> reasonCustomerId) {
        this.reasonCustomerId = reasonCustomerId;
    }

}