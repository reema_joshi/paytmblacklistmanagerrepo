package com.pfg.BlackListManager.response;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {

    private String status;
    private Boolean blocked;
    private long blacklisted;
    private String message;
    private String blackListedBy;

    public long getDeletedCount() {
        return deletedCount;
    }

    public void setDeletedCount(long deletedCount) {
        this.deletedCount = deletedCount;
    }

    private long deletedCount;

    public Response(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public Response() {
    }

    public Response(String status) {
        this.status = status;
    }

    public Response(String success, long deletedCount) {
        this.status = success;
        this.deletedCount = deletedCount;
    }

    public long getBlacklisted() {
        return blacklisted;
    }

    public void setBlacklisted(long blacklisted) {
        this.blacklisted = blacklisted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public void setBlackListedBy(String blackListedBy) {
        this.blackListedBy = blackListedBy;
    }

    public String getBlackListedBy() {
        return blackListedBy;
    }
}
